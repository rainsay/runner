# GitLab-Runner 的安装（Docker）

> [官方文档](https://docs.gitlab.com/runner/register/)

## 1. 下载 gitlab-runner 镜像

> `docker pull gitlab-runner`

## 2. 启动容器

```bash
docker run --rm -t -i --name gitlab-runner -d \
-v /var/run/docker.sock:/var/run/docker.sock \
-v /srv/gitlab-runner/config:/etc/gitlab-runner \
 gitlab/gitlab-runner register

# 进行注册
## 第一步：填写gitlab服务器地址
Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com )
https://gitlab.com

## 第二步：填写你的访问token
Please enter the gitlab-ci token for this runner
xxx

## 第三步：描述信息
Please enter the gitlab-ci description for this runner
my-runner

## 第四步：该runner的标签，可在 .gitlab-ci.yml 文件中使用指令 tags 使用
Please enter the gitlab-ci tags for this runner (comma separated):
my-tag,another-tag

## 第五步：选择执行环境
Please enter the executor: ssh, docker+machine, docker-ssh+machine, kubernetes, docker, parallels, virtualbox, docker-ssh, shell:
docker

## 第六步：选择 excutor 的执行镜像
Please enter the Docker image (eg. ruby:2.1):
maven:3.3.9-jdk-8
```

## 3. gitlab runner 进行相关配置

我们启动gitlab runner 时，挂载了一个 `/srv/gitlab-runner/config:/etc/gitlab-runner` 的 volume，里面有 gitlab-runner的配置文件 `config.toml`

```toml
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "t"st
  url = "https://gitlab.com/"
  token = "xxxxxxx"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.docker]
    tls_verify = false
    image = "maven:3.3.9-jdk-8"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache", "/root/.m2:/root/.m2:rw"]
    shm_size = 0
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
```

由于每次构建，gitlab runner 都会运行一个全新的 excutor，所以为避免每次构建时，excutor 都去线上下载依赖包，这里我们为 excutor 配置挂载了本地的 `/root/.m2:/root/.m2`。但由于 excutor 的镜像为`maven:x.x.x-jdk`，他默认的maven repository 在 `/usr/share/maven/ref/repository`, 所以我们需要更改`/root/.m2/docker-setting.xml`文件，把 localRepository 改为 /root/.m2/repository

### Gitlab Runner Cache
还有一种避免每次下载依赖包的方法，就是使用 gitlab runner cache

使用cache时，需要在 .gitlab-ci.yml 文件中使用指令 cache，如：
```yml
image: maven:3.3.9-jdk-8

cache:
  key: maven-repository-cache
  paths:
    - .m2

variables:
  MAVEN_OPTS: "-Dmaven.repo.local=.m2"
 
stages:
  - build
  
build:
  stage: build
  script:
    - mvn clean install -DskipTests
  tags:
    - test
```

cache 位于 excutor 的 `/cache` 文件夹下，使用时我们需要指定 path 和 key。
当我们构建时，如果 key 指定的内容不存在，则会构建完成后把 path下的内容打包成 cache.zip
如果刚开始构建时， key 命中，则会使用 path 里面的内容

每次构建完，其产物会保存在 excutor 的`/build`文件夹下
